#!/usr/bin/env bash

# _   _ _     _        ______           
#| | | | |   (_)      |___  /               UbioZur / ubiozur.tk
#| | | | |__  _  ___     / / _   _ _ __     
#| | | | '_ \| |/ _ \   / / | | | | '__|    Wallpapers Convertion script
#| |_| | |_) | | (_) |./ /__| |_| | |           Convert and dispatch the wallpaper images
# \___/|_.__/|_|\___/ \_____/\__,_|_|       
#

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"


# Check that image magick is installed
convert="$(command -v convert)"
if [ ! -x $convert ]; then
	echo -e "\033[0;35m[WARNING] Cannot find Imagemagick\033[0;0m"
    
    aptget="$(command -v apt-get)"
    if [ ! -x $aptget ]; then
        # install imagemagick with apt-get
        echo -e "\033[0;34m[LOG]\033[0;0m     Installing Imagemagick"
        sudo aptget install imagemagick
    else
        echo -e "\033[1;91m[ERROR]   Make sure Imagemagick is installed\033[0;0m"
        exit 1
    fi
fi

# Folder from which to convert the images (Full path)
FROMFOLDER="$SCRIPT_DIR/toconvert"

# TODO: Check that there are images in the folder!

# List of output folders (Folder name) which will determine the image size

TOFOLDERS=($(/usr/bin/ls -d * | /usr/bin/grep -E '[0-9]{3,}x[0-9]{3,}'))

# Loop through the folders to resize the images
for size in "${TOFOLDERS[@]}"
do
    echo "Processing $size"
    # Grab the folder width and height
    width="$(echo "$size" | cut -d 'x' -f1)"
    height="$(echo "$size" | cut -d 'x' -f2)"

    # Only select images that are bigger than the resolution for that folder
    
    # Get files width, height and filename
    # only print filenames where width and heights are above or equal to the folder one
    # replace newlines by null characters
    # place the list as argument name {} and resize to fill the area
    # center the image and force the image to be of that dimention (crop)
    # set quality and copy the image with the proper filename in the folder

    identify -format '%w %h %i\n' "$FROMFOLDER/*.*" |                                               
    awk '$1 >= "$width" && $2 >= "$height" {sub(/^[^ ]* [^ ]* /, ""); print}' |
    tr '\n' '\0' |
    xargs -0 -I '{}' convert {} -resize "${width}x${height}^" \
                     -gravity center -extent "${width}x${height}" \
                     -quality 60% -set filename:f '%t.%e' +adjoin "$SCRIPT_DIR/$size/%[filename:f]"
done

# prompt to delete the images
#echo -e -n "\033[0;35m --- Type yes to confirm --- \033[0;0m"
#echo -e "\033[0;35m[CONFIRM]\033[0;0m Delete the files in the folder: $FROMFOLDER (type yes to continue)"
#read
#if [ "$REPLY" == "yes" ]; then
#    # TODO: Fix /usr/bin/rm: cannot remove '/home/quentin/Pictures/wallpapers/toconvert/*.*': No such file or directory
#    /usr/bin/rm -v "$FROMFOLDER/*.*"
#fi
