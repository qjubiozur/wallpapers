# UbioZur System Wallpapers

My wall papers from my own photos.

The wall papers are organized by resolution so it's easier to only download the wanted resolution with a git sparse checkout!

## Convertion

Each folder represent a screen resolution to have the wallpapers optimized for.

Add the images to the `toconvert` folder.

Run the `convert.sh` script to convert the images to the resolutions and move them to the folder.

CAUTION: the script will overwrite the images in the destination folder without asking!
